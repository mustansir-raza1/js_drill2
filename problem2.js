//2. Convert all the salary values into proper numbers instead of strings.

function getProperSalaryDetails(employees){
    if(Array.isArray(employees)){
        let properNumber = employees.map((person) => {
            person.salary= parseFloat(person.salary.replace('$',''));
            return person;
        });
        return properNumber;
    }
    else{
        return [];
    }
}

module.exports = getProperSalaryDetails;
