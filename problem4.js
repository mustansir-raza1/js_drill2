// Find the sum of all salaries.
function sumOfSalaries(employees){
    if(Array.isArray(employees)){
        let sumOfpay = employees.reduce((acc, next)=> acc + next.changeSalary, 0);
        return sumOfpay
    }
    else{
        return [];
    }
}
module.exports = sumOfSalaries