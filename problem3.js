//3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function getModifiedSalary(employees){
    if(Array.isArray(employees)){
        let changeSalary = employees.map((person) => {
            person.changeSalary = person.salary*10000;
            return person;
        });
        return changeSalary;
    }
    else{
        return [];
    }
}

module.exports = getModifiedSalary;
