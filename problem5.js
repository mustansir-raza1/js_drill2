//Find the sum of all salaries based on country. ( Group it based on country and then find the sum )

function sumOfSalariesCountry(employees){
    if(Array.isArray(employees)){
        let totalSalary = employees.reduce((add, next) =>{
            let country = next.location;
            if(!add[country]){
                add[country] = 0;
            }
            add[country] += next.changeSalary;
            return add;
        },{});
        return totalSalary
    }
    else{
        return [];
    }
}
module.exports = sumOfSalariesCountry;