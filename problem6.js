// Find the average salary of based on country.
function averageSalary (employees){
    if (Array.isArray(employees)){
        let totalSalary = employees.reduce((previous, next) =>{
            let country = next.location;
            if(!previous[country]){
                previous[country] = 0;
            }
            previous[country] += next.changeSalary;
            return previous;
        },{});
        const countryCount = employees.reduce((previous,next) =>{
            let country = next.location;
            if(!previous[country]){
                previous[country] = 0;
            }
            previous[country] += 1;
            return previous;
        },{});
        let averageSalariesCount = {};
        for(const val in totalSalary){
            averageSalariesCount[val] = totalSalary[val] / countryCount[val];
        }
        return averageSalariesCount
    }
    else{
        return [""];
    }
}
module.exports = averageSalary;