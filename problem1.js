// Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function getAllWebDeveloper(employees){
    if(Array.isArray(employees)){
        let webDevelopers = employees.filter((person) => person.job.includes("Web Developer"));
        return webDevelopers;
    }
}

module.exports = getAllWebDeveloper;
